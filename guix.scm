(use-modules (guix profiles)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages pkg-config))

(packages->manifest
 (list autoconf
       automake
       gnu-make
       guile-next
       pkg-config))
