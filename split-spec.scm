;;; guile-cloudformation --- Scheme DSL for CloudFormation templates
;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;;
;;; Guile-CloudFormation is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-CloudFormation is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (aws cloudformation utils)
             (aws cloudformation utils json)
             (ice-9 match)
             (srfi srfi-1))

(define spec
  (call-with-input-file "cloudformation-spec.json" read-json))
(define spec-version (assoc-ref spec "ResourceSpecificationVersion"))
(define properties-by-service (make-hash-table))
(define resources-by-service (make-hash-table))

(define (parse-service-name name)
  (match (delete "" (string-split name #\:))
    (("Tag") "Universal")
    ((_ service . _) service)))

(define (add-to-group table type-spec)
  (match type-spec
    ((name . details)
     (let* ((service (parse-service-name name))
            (existing-stuff (or (hash-ref table service) '())))
       (hash-set! table
                  service
                  (cons type-spec existing-stuff))))))

(for-each (lambda (property)
            (add-to-group properties-by-service property))
          (assoc-ref spec "PropertyTypes"))
(for-each (lambda (property)
            (add-to-group resources-by-service property))
          (assoc-ref spec "ResourceTypes"))

(define (hash-table-keys table)
  (hash-fold (lambda (k v memo) (cons k memo)) '() table))

(define services
  (sort (lset-union string=?
                    (hash-table-keys properties-by-service)
                    (hash-table-keys resources-by-service))
        string<))

(for-each (lambda (service)
            (let* ((module-suffix (symbol->string (aws-string->symbol service)))
                   (file-name (string-append "aws/cloudformation/"
                                             module-suffix ".cfn")))
              (display "create ")
              (display file-name)
              (newline)
              (call-with-output-file file-name
                (lambda (port)
                  (let ((properties (or (hash-ref properties-by-service service)
                                        '()))
                        (resources (or (hash-ref resources-by-service service)
                                       '())))
                    (write `(("ModuleSuffix" . ,module-suffix)
                             ,@(if (string=? service "Universal")
                                   `(("ResourceSpecificationVersion" .
                                      ,spec-version))
                                   '())
                             ("PropertyTypes" . ,properties)
                             ("ResourceTypes" . ,resources))
                           port))))))
          services)
