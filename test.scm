(use-modules (aws cloudformation)
             (aws cloudformation ec2)
             (aws cloudformation s3)
             (aws cloudformation utils json)
             (oop goops))

(define security-group
  (make <security-group>
    #:id 'security-group
    #:group-description "CFN Test"
    #:vpc-id "vpc-redacted"
    #:security-group-egress (list
                             (make <security-group/egress>
                               #:ip-protocol "tcp"
                               #:cidr-ip "0.0.0.0/0"
                               #:from-port 22
                               #:to-port 22))
    #:security-group-ingress (list
                              (make <security-group/ingress>
                                #:ip-protocol "tcp"
                                #:cidr-ip "0.0.0.0/0"
                                #:from-port 0
                                #:to-port 65535))))

(define ec2-instance
  (make <instance>
    #:id 'instance
    #:image-id "ami-0bbe6b35405ecebdb"
    #:key-name "dthompson"
    #:instance-type "t3.nano"
    #:security-group-ids (list security-group)
    #:tags (list (make <tag> #:key "Name" #:value "CFN Test"))))

(define bucket
  (make <bucket>
    #:id 'bucket
    #:bucket-name "foo.example.com"))

(define stack
  (make <cloudformation-stack>
    #:resources (list ec2-instance bucket)))

(write-json (to-json stack) (current-output-port))
(newline)
